
function LightBox(options){
    this.$el = $("#" + options.id);
    this.$window = $(window);
    this.width = this.$window.width();
    this.height = this.$window.height();
    this.$item = this.$el.children();
    this.speed = options.speed;
    this.index = 0;
    this.count = 0;
    this.position = 0;
    this.error = options.error || function error(){};
    this.endScroll = options.endScroll || function endScroll(){}
    this.init();
    //console.log(this.endScroll)
}

 LightBox.prototype.init = function(){
    $items = this.$item;
    this.count = $items.length;
    $items.css({
        "width": this.width,
        "height": this.height
    })
    this.go(0);
}
 LightBox.prototype.lazyLoadImg = function(img){
    var width = this.$window.width();
    var height = this.$window.height();
    var _this = this;
    if(!img.attr("_src")){
        return;
    }
    var src = img.attr("_src");
    var image = new Image();
    //console.log(img)
    image.onload = function(){
       var mWidth = width > this.width ? this.width : width;
       var mHeight = 0;
       if(this.width > width){
        //console.log("1")
        mHeight = width / this.width * this.height;
       }else{
        mHeight = this.height;
       }
       //alert(mHeight)
       img.data({"width": this.width, "height": this.height}).css({
           width: mWidth,
           height: mHeight
       })
    }
    image.src = src;
    img[0].src = src;
    //console.log(_this);
    _this.endScroll(this.index, _this);
}
 LightBox.prototype.getDescription = function(index){
    return $(this.$item[index]).find("p").html();
}
 LightBox.prototype.next = function(){
    this.index++
    if( this.index >= this.count){
        this.index = this.count - 1;
        this.error("已经是最后一个了");
        return;
    }
    this.go(this.index++);
}
 LightBox.prototype.prev = function(){
    this.index--;
    if( this.index < 0){
        this.index = 0;
        this.error("已经是第一个了");
        return;
    }
    this.go(this.index--);
}
 LightBox.prototype.go = function(index){
       //console.log(index);
        var _this = this;
        this.index = index;
        var left = 0;
        for(var i = 0; i < this.count; i++){
            if(i >= index){
                break;
            }
            left+= $(this.$item[i]).width();
        }
        this.position = -left;
        this.lazyLoadImg($(this.$item[index]).find("img"));

        this.$el.animate({left: this.position}, this.speed ,function  (argument) {
           _this.endScroll({
            index: _this.index,
            count: _this.count
        }, _this); // body...
        });
        
}
 LightBox.prototype.resize = function() {
    var _this = this;
    var width = this.$window.width();
    var height = this.$window.height();
    this.$item.css({
        "width": width,
        "height": height
    })
    var $img = $(this.$item[this.index]).find("img");
    var imgWidth = $img.data("width");
    var imgHeight = $img.data("height");

    var w = width > imgWidth ? imgWidth : width;
    if(width > w){     
        h = imgHeight;
    }else{
       h = (w / imgWidth) * imgHeight;
    }
    //var h = height > imgHeight ?   (width / imgWidth) * height : imgHeight;
    $img.css({
        width: w,
        height: h
    })
    index = this.index;
    var left = 0;
    for(var i = 0; i < this.count; i++){
        if(i >= index){
                break;
        }
        left+= $(this.$item[i]).width();
    }
    this.position = -left;
    this.$el.css("left", this.position);
}
