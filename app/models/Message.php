<?php

class Message extends Eloquent {
	public function getType()
	{
		$types = ['share'=>'投递故事', 'join'=>'参与拍摄'];
		return $types[$this->type];
	}
}