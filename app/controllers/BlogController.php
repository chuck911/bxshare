<?php

class BlogController extends BaseController {

    protected $post;

    protected $user;

    public function __construct(Post $post, User $user)
    {
        parent::__construct();

        $this->post = $post;
        $this->user = $user;
    }

	public function getIndex()
	{
		$posts = $this->post->orderBy('created_at', 'DESC')->paginate(10);
		return View::make('site/blog/index', compact('posts'));
	}

	public function getView($slug)
	{
		$post = $this->post->where('slug', '=', $slug)->first();

		if (is_null($post))
		{
			return App::abort(404);
		}

		$comments = $post->comments()->orderBy('created_at', 'ASC')->get();

        $user = $this->user->currentUser();
        $canComment = false;
        if(!empty($user)) {
            $canComment = $user->can('post_comment');
        }

		return View::make('site/blog/view_post', compact('post', 'comments', 'canComment'));
	}

	public function postView($slug)
	{

        $user = $this->user->currentUser();
        $canComment = $user->can('post_comment');
		if ( ! $canComment)
		{
			return Redirect::to($slug . '#comments')->with('error', 'You need to be logged in to post comments!');
		}

		$post = $this->post->where('slug', '=', $slug)->first();

		$rules = array(
			'comment' => 'required|min:3'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{
			$comment = new Comment;
			$comment->user_id = Auth::user()->id;
			$comment->content = Input::get('comment');

			if($post->comments()->save($comment))
			{
				return Redirect::to($slug . '#comments')->with('success', 'Your comment was added with success.');
			}

			return Redirect::to($slug . '#comments')->with('error', 'There was a problem adding your comment, please try again.');
		}

		return Redirect::to($slug)->withInput()->withErrors($validator);
	}
}
