<?php

class AdminMessagesController extends AdminController
{
	public function getIndex()
	{
		$title = '所有留言';
        $messages = (new Message)->orderBy('created_at', 'DESC')->paginate(10);
        return View::make('admin/messages/show', compact('messages', 'title'));
	}

	public function getShow($messageId)
	{
		$title = '所有留言';
		$currentMessage = Message::find($messageId);
		$messages = (new Message)->orderBy('created_at', 'DESC')->paginate(10);
        return View::make('admin/messages/show', compact('currentMessage','messages', 'title'));
	}

	public function getDelete($messageId)
	{
		$message = Message::find($messageId);
		if ($message) $message->delete();
        return Redirect::back()->with('success','删除成功');
	}
}