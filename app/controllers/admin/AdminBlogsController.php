<?php

class AdminBlogsController extends AdminController {

    protected $post;

    public function __construct(Post $post)
    {
        parent::__construct();
        $this->post = $post;
    }

    public function getIndex()
    {
        $title = '所有故事';
        $posts = $this->post->orderBy('created_at', 'DESC')->paginate(10);
        return View::make('admin/blogs/index', compact('posts', 'title'));
    }

	public function getCreate()
	{
        $title = Lang::get('admin/blogs/title.create_a_new_blog');
        $uploadToken = $this->uploadToken();
        return View::make('admin/blogs/create_edit', compact('title','uploadToken'));
	}

    private function uploadToken()
    {
        $cloud = new QiniuClient('OLm4mclHS5rcwvhXjwa4VU3LNl0aU--7Lema068_','urEiDu9D26WtBMvB9G3zKusdECuSBn3YyCK8zIkx');
        return $cloud->uploadToken(['scope'=>'bxstory']);
    }

	public function postCreate()
	{
        $rules = array(
            'title'   => 'required|min:3',
            'content' => 'required|min:3'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $user = Auth::user();

            $this->post->title            = Input::get('title');
            $this->post->slug             = Str::slug(Input::get('title'));
            $this->post->content          = Input::get('content');
            $this->post->meta_title       = Input::get('meta-title');
            $this->post->meta_description = Input::get('meta-description');
            $this->post->meta_keywords    = Input::get('meta-keywords');
            if (Input::get('image-url')) $this->post->image = Input::get('image-url');
            $this->post->intro            = Input::get('intro');
            $this->post->video            = Input::get('video');
            $post->type                   = Input::get("type");
            $this->post->user_id          = $user->id;

            if($this->post->save())
            {
                return Redirect::to('admin/blogs/' . $this->post->id . '/edit')->with('success', Lang::get('admin/blogs/messages.create.success'));
            }

            return Redirect::to('admin/blogs/create')->with('error', Lang::get('admin/blogs/messages.create.error'));
        }

        return Redirect::to('admin/blogs/create')->withInput()->withErrors($validator);
	}

	public function getShow($post)
	{
        // redirect to the frontend
	}

	public function getEdit($post)
	{
        $title = Lang::get('admin/blogs/title.blog_update');
        $uploadToken = $this->uploadToken();
        return View::make('admin/blogs/create_edit', compact('post', 'title', 'uploadToken'));
	}

	public function postEdit($post)
	{

        $rules = array(
            'title'   => 'required|min:3',
            'content' => 'required|min:3'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $post->title            = Input::get('title');
            $post->slug             = Str::slug(Input::get('title'));
            $post->content          = Input::get('content');
            $post->meta_title       = Input::get('meta-title');
            $post->meta_description = Input::get('meta-description');
            $post->meta_keywords    = Input::get('meta-keywords');
            $post->type             = Input::get("type");
            $post->intro            = Input::get('intro');
            $post->video            = Input::get('video');
            if (Input::get('image-url')) $post->image = Input::get('image-url');

            if($post->save())
            {
                return Redirect::to('admin/blogs/' . $post->id . '/edit')->with('success', '故事保存成功');
            }

            return Redirect::to('admin/blogs/' . $post->id . '/edit')->with('error', Lang::get('admin/blogs/messages.update.error'));
        }

        return Redirect::to('admin/blogs/' . $post->id . '/edit')->withInput()->withErrors($validator);
	}

    public function getStar($post)
    {
        $post->star = 1;
        $post->save();
        return Redirect::back();
    }

    public function getUnstar($post)
    {
        $post->star = 0;
        $post->save();
        return Redirect::back();
    }

    public function getPublish($post)
    {
        $post->status = 1;
        $post->save();
        return Redirect::back();
    }

    public function getUnpublish($post)
    {
        $post->status = 0;
        $post->save();
        return Redirect::back();
    }

    public function getDelete($post)
    {
        $post->delete();
        return Redirect::back()->with('success','删除成功');
    }

    public function postImage()
    {
        $image = Input::file('upload_file');
        $validator = Validator::make(['image' => $image], ['image' => 'image']);
        if ($image->isValid() && !$validator->fails()) {
            $filename = md5(uniqid(rand(), true)).'.'.$image->getClientOriginalExtension();
            $image->move('assets/upload/', $filename);
            return Response::json(['file_path'=>'/assets/upload/'.$filename]);
        }
    }
}