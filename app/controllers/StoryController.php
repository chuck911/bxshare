<?php


class StoryController extends BaseController {
	

	public function getIndex()
	{
		$detect = new Mobile_Detect;
		$isMobile = $detect->isMobile();
		$starPosts = Post::where('star',1)->orderBy('created_at','desc')->take(5)->get();
		
		$posts = Post::where('status', 1)->where('star', 1)->orderBy('created_at','desc')->take(9)->get();
		return View::make('site/story/index', compact('starPosts', 'posts', 'isMobile'));
	}

	public function postMessage()
	{
		$rules = [
			'name'   => 'required',
			'contact' => 'required',
			'content' => 'required'
		];
		$message = new Message;
		$message->content = Input::get('content');
		$message->contact = Input::get('contact');
		$message->name = Input::get('name');
		$message->type = Input::get('type');
		if (!$message->save()) {
			App::abort(400,'请完整填写信息');
		}
	}
	public function getList(){
		$posts = Post::where('status',1)->orderBy('created_at','desc')->get();
		return View::make('site/story/list', compact('posts'));
	}

	public function getShow($post)
	{	
		$detect = new Mobile_Detect;
		$isMobile = $detect->isMobile();
		$count = 0;
		if($isMobile == 1 || $post["type"] == 1){
			return View::make('site/story/show', compact('post'));
		}else{
			$content = explode("<hr>", $post["content"]);
			foreach ($content as $key => $value) {
				phpQuery::newDocument($value);			
				$src = pq('img')->attr('src');				
				if($src == ""){					
					$text = $value;
				}else{					
					$text = pq('p')->text();					
					$text = '<p>'.$text.'</p>';
					$count++;
				}
				$content[$key] =  array('text' => $text,'img'=> $src );				
			}
			$post["content"] = $content;
			$post["count"] = $post->video ? $count + 1 : $count;
			return View::make('site/story/slide', compact('post'));
		}
		
	}
	
}