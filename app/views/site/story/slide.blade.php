@extends('site.layouts.base')

@section('styles')
<style>
body {
	background: #f7f7f7;
}
</style>
@stop

@section('title')
{{{ $post->title }}} ::
@parent
@stop

@section('content')
<div class="page-container">
	<a  class="arrow-left"></a>
    <a  class="arrow-right"></a>
    <div class="lightBox-container">

        <div class="lightBox" id="lightBox">
        		@if ($post->video)
        		<div class="lightBox-content video-content" data-inde="0">
        		<div class="video-box">
        		<div class="story-video embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="http://player.youku.com/embed/{{{ $post->videoPlayer() }}}" frameborder=0 allowfullscreen></iframe>
				</div>
        		</div>
				
				</div>
				@endif
        		@foreach($post->content as $key => $value)
        		@if($value["img"]!= "")
                <div class="lightBox-content" data-index="{{$post->video ? $key + 1: $key}}">
               
                    <div class="box-middle">
                        <div class="img-box">
                            <img _src="{{{$value["img"]}}}"  >
                        </div>
                        {{$value["text"]}}
                    </div>
                </div> 
                @else
                  <div class="lightBox-content last-item" data-index="{{$post->video ? $key + 1: $key}}">
                  	<div class="text-content-container">
                  		<div class="text-content">
                  			{{$value["text"]}}
                  		</div>
                        <div class="page-nav">
                            <a href="/">首页</a>
                            <span>></span>
                            <a href="/story/list">更多故事</a>
                        </div>
                  		<div class="jiathis_style" style="overflow:hidden;>
						<a class="jiathis_button_qzone"></a>
						<a class="jiathis_button_tsina"></a>
						<a class="jiathis_button_tqq"></a>
						<a class="jiathis_button_weixin"></a>
						<a class="jiathis_button_renren"></a>
						<a href="http://www.jiathis.com/share" class="jiathis jiathis_txt jtico jtico_jiathis" target="_blank"></a>
						<a class="jiathis_counter_style"></a>
					</div>
					<script type="text/javascript" src="http://v3.jiathis.com/code/jia.js?uid=1394082829506107" charset="utf-8"></script> 
                   	</div>
                  	
                  	
                  </div> 

               	@endif
                
                @endforeach
            </div>
            <div class="lightBox-description-container">
                <div class="pic-desc-box">
                    <div class="pic-desc">
                        <div class="page-num">
                        	<div class="curr-num"></div>
                        	<div class="total-num">{{$post->count}}</div>
                        </div>
                        <div class="detail">
                            <p></p>
                        </div>
                        <a href="javascript:;" class="button close-btn" id="close-desc"></a>
                    </div>
                    <div class="open-btn-box">
                        <a href="javascript:;" class="button open" id="open-desc">展开</a>
                    </div>
                </div>


            </div>
        </div>    
</div>
@stop
@section("scripts")
<script src="{{ asset('assets/js/lightBox.js')}}"></script>
<script type="text/javascript">
	$(function(){
	    var index = 0;
	    var count = 0;
       	var $w = $(window),
            $topbar = $("#topbar"),
            $footer = $("#footer"),
            $currNum = $(".curr-num"),
            $desc = $(".lightBox-description-container");

       	var $pageContainer =  $(".page-container");
       	var topHeight = $topbar.outerHeight() + $footer.outerHeight();
        $pageContainer.height($w.height());
        $pageContainer.width($w.width());
        var lightBox = new LightBox({
            id:"lightBox",
            speed:200,
            endScroll:function(result, self){
            	index = result.index;
            	count = result.count;
                var desc = self.getDescription(result.index);
                $(".detail p").html(desc);
                $currNum.html(result.index + 1);
                var last = $(".last-item").data("index");
                if(last == result.index){
                	$topbar.show();
        			$footer.show();
        			$desc.hide();
        			$pageContainer.css({"height":$w.height() -  topHeight});
                }else{
                	$topbar.hide();
        			$footer.hide();
        			$desc.show();
        			$pageContainer.css({"height":$w.height()});
                }
            },
            error:function(msg){
                var $tips = $('<div class="tips"><p>'+ msg+'</p></div>');
                if($(".tips").lenght > 0){
                	return;
                }
                $("html, body").append($tips);
                setTimeout(function () {
                	$(".tips").remove();
                },2000)

            }
        })
        $(".arrow-left").on("click",function(){
            lightBox.prev();
        })
        $(".arrow-right").on("click",function(){
            lightBox.next();
        })
        $("#close-desc").on("click", function(){
            $(".open-btn-box").css({"display":"block"});
            $(this).hide();
            $(".detail").hide();
            $(".page-num").hide();
        })
        $("#open-desc").on("click", function(){
            $("#close-desc").show();
            $(".open-btn-box").hide();
            $(".detail").show();
            $(".page-num").show();
        })
        var isOn = false;
        var $detail = $(".detail");
        $detail.height("48px");
        $(".pic-desc-box").mouseover(function (argument) {
        	// body...
        	if(isOn){
        		return;
        	}
        	var height = $("p", $detail).height();
        
        	if(height < 48){
        		return;
        	}
        	$detail.animate({"height":height},300);
        	isOn = true;
        }).mouseout(function() {
        	$detail.animate({"height": "48px"},300);
        	isOn = false;
        })

        $(window).on("keyup", function (event) {
        	if(event.which == 37){
        		lightBox.prev();
        	}else if(event.which == 39){
        		lightBox.next();
        	}
        }).on("resize", function  (argument) {
        	$pageContainer.height($w.height());
        	$pageContainer.width($w.width());
        	var height = 0;
        	if(index == count -1){
        		height = $w.height()- topHeight;
        	}else{
        	 	height = $w.height();
        	}
        	$pageContainer.css({"height":height});
        	lightBox.resize();
        })

        
    })
</script>
@stop
