@extends('site.layouts.base')

@section('styles')
<style>
body {
	background: #f7f7f7;
}
</style>
@stop

@section('title')
{{{ $post->title }}} ::
@parent
@stop

@section('content')
<div class="container">
	<h2 class="story-title">{{{ $post->title }}}</h2>
	<div class="story-content">
		@if ($post->video)
		<div class="story-video embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="http://player.youku.com/embed/{{{ $post->videoPlayer() }}}" frameborder=0 allowfullscreen></iframe>
		</div>
		@endif
		<div class="story-text">
			{{ $post->content }}
		</div>
		<!-- JiaThis Button BEGIN -->
		<div class="jiathis_style" style="margin:15px 0 5px;overflow:hidden;">
			<a class="jiathis_button_qzone"></a>
			<a class="jiathis_button_tsina"></a>
			<a class="jiathis_button_tqq"></a>
			<a class="jiathis_button_weixin"></a>
			<a class="jiathis_button_renren"></a>
			<a href="http://www.jiathis.com/share" class="jiathis jiathis_txt jtico jtico_jiathis" target="_blank"></a>
			<a class="jiathis_counter_style"></a>
		</div>
		<script type="text/javascript" src="http://v3.jiathis.com/code/jia.js?uid=1394082829506107" charset="utf-8"></script>
		<!-- JiaThis Button END -->
	</div>
</div>
@stop