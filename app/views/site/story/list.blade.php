@extends('site.layouts.base')

@section('content')
	<div class="container" id="stories">

		<div class="row">
			@foreach ($posts as $post)
				<div class="col-md-4 col-sm-6 col-xs-12">
					<a href="{{{ URL::to('story/'.$post->id) }}}" target="_blank" class="story-item">
						@if ($post->image)
							<img src="{{{ $post->image }}}!300x200">
						@else
							<img src="http://lorempixel.com/300/200/people/">
						@endif
						<div class="title">{{{ $post->title }}}</div>
						<div class="description"> {{{ $post->intro }}} <span class="icon-play">&#9658;</span></div>
					</a>
				</div>
			@endforeach
		</div>
	</div>
	
	
@stop
