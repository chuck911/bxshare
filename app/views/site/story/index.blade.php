@extends('site.layouts.base')

@section('content')
	@if(count($starPosts) && $isMobile == 1)
	<div id="slideshow">
		<div class="container">

			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					@for ($i = 0; $i < count($starPosts); $i++)
					<li data-target="#carousel-example-generic" data-slide-to="{{{ $i }}}" class="@if($i==0) active @endif"></li>
					@endfor
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					@foreach ($starPosts as $i => $starPost)
					<a target="_blank" class="item @if($i==0) active @endif" href="{{{ URL::to('story/'.$starPost->id) }}}">
						<img src="{{{ $starPost->image }}}!940x517">
						<div class="carousel-label">
							<div class="title">{{{ $starPost->title }}}</div>
						</div>
					</a>
					@endforeach
				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>
	</div>
	@else
	<div id="slideshow">
		<div class="slide-container">
			<div class="slide-inner" id="slide">
				@foreach ($starPosts as $i => $starPost)
				<a target="_blank" class="item @if($i==0) active @endif" href="{{{ URL::to('story/'.$starPost->id) }}}">
					<img src="{{{ $starPost->image }}}!940x517">
					<div class="carousel-label">
						 <div class="title">{{{ $starPost->title }}}</div>
					</div>
				</a>
				@endforeach
				
			</div>
			<a class="left carousel-control" href="#" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>	
		
		
	</div>	
	@endif
	<div class="container" id="actions">
		<div class="row">
			<div class="col-md-6">
				<a class="action" href="#" data-toggle="modal" data-target="#modal-share">
					我有故事，我希望分享给大家
					<span class="go">Go</span>
				</a>
			</div>
			<div class="col-md-6">
				<a class="action" href="#" data-toggle="modal" data-target="#modal-join">
					我会拍摄，我希望参与拍摄
					<span class="go">Go</span>
				</a>
			</div>
		</div>
	</div>
	<div class="container" id="stories">
		<h3><a href="/story/list">更多精彩故事</a></h3>
		<div class="row">
			@foreach ($posts as $post)
				<div class="col-md-4 col-sm-6 col-xs-12">
					<a href="{{{ URL::to('story/'.$post->id) }}}" target="_blank" class="story-item">
						@if ($post->image)
							<img src="{{{ $post->image }}}!300x200">
						@else
							<img src="http://lorempixel.com/300/200/people/">
						@endif
						<div class="title">{{{ $post->title }}}</div>
						<div class="description"> {{{ $post->intro }}} <span class="icon-play">&#9658;</span></div>
					</a>
				</div>
			@endforeach
		</div>
	</div>
	<div class="modal fade modal-message" id="modal-share" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">我有故事，我希望分享给大家</h4>
				</div>
				<form class="form-horizontal" role="form">
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
					<input type="hidden" name="type" value="share" />
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-2 control-label">姓名</label>
							<div class="col-sm-6">
								<input name="name" required class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">联系方式</label>
							<div class="col-sm-6">
								<input name="contact" required class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">故事简述</label>
							<div class="col-sm-10">
								<textarea name="content" required class="form-control" rows="10" placeholder="故事样本：交易发生在2014年4月份，我在百姓网上看中了一把二手电子琴。和卖家约好周末前去他家里取琴。
在和卖家交流中得知，他和我都是做IT这一行，而且也都喜欢音乐。
他还提到当初买这把琴是为了弹给前女友听，但现在两人分手了，他索性就把琴卖了。
我当时急于回家并没有多听他的故事。走前，卖家还帮忙调了音，仔细包装好琴。
但交易之后我们没有了联系，我庆幸自己遇到这样充满人情味的交易，也懊悔当时没耐心听哥们讲完琴背后的故事，感觉自己可能错过了一个朋友。"></textarea>
								<div class="alert alert-info" role="alert" style="margin:15px 0 0">
									1. 故事发生在百姓网的交易过程中<br>
									2. 故事是真实的<br>
									3. 请留下您的联系方式，如果故事合适，我们会尽快通知您
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary">发送</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade modal-message" id="modal-join" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">我会拍摄，我希望参与拍摄</h4>
				</div>
				<form class="form-horizontal" role="form">
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
					<input type="hidden" name="type" value="join" />
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-2 control-label">姓名</label>
							<div class="col-sm-6">
								<input name="name" required class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">联系方式</label>
							<div class="col-sm-6">
								<input name="contact" required class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">自我介绍</label>
							<div class="col-sm-10">
								<textarea name="content" required class="form-control" rows="5"></textarea>
							</div>
						</div>
						<div class="alert alert-info" role="alert" style="margin:15px 0 0">
							<h5>如果成功参与拍摄，您将获得：</h5>
							1. 百姓故事视频署名，工作室或个人均可；<br>
							2. 视频将获得百姓网千万用户，日均百万级流量的关注和点击；<br>
							3. 百姓网官方微信定向推送，覆盖全国几十万中小商家，获得其他拍摄商机；<br>
							4. 如能入选百姓网十周年优秀故事，将有机会获得百姓网专属礼品和奖项。
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary">发送</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-success" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
				发送成功
				</div>
			</div>
		</div>
	</div>
@stop
@section('scripts')

	<script src="{{ asset('assets/js/slide.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){ 
        	var slide = new Slide({
            	id : "slide",
            	height: "400",
        		width: "800"
        	})
        	$(".glyphicon-chevron-right").on("click", function (event) {
        		event.preventDefault();
        		slide.next();
        	})
        	$(".glyphicon-chevron-left").on("click", function (event) {
        		event.preventDefault();
        		slide.prev();
        	})
    	})
		$('.modal-message form').on('submit',function(event){
			event.preventDefault();
			var $form = $(this);
			$.post('/message',$(this).serialize(),function(){
				$form.parents('.modal').modal('hide');
				$('#modal-success').modal('show');
			});
		});
	</script>
@stop