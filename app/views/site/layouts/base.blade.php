<!DOCTYPE html>
<html>
<head>
	<title>
		@section('title')
		百姓故事
		@show
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="http://cdn.staticfile.org/twitter-bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/app.css') }}}">
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
	<![endif]-->
	<link rel='shortcut icon' href='http://s.baixing.net/favicon.ico' type='image/x-icon'>
	@yield('styles')
</head>
<body>
	<div id="topbar">
		<div class="container">
			<a class="brand" href="/">
				<img src="http://s.baixing.net/img/visualize/logo_81x36.png">  
				<span id="site-name">百姓故事</span>
			</a>
		</div>
	</div>
	@yield('content')
	<div id="footer">
		<div class="container">
			百姓网  hezuo@baixing.com  QQ:4324324 &nbsp;&nbsp;&nbsp;
			<a class="icp" href="http://www.miitbeian.gov.cn" target="_blank">
          	沪ICP备06019413号 沪ICP证B2-20080088
        	</a>
		</div>
	</div>
	
	<script type="text/javascript" src="http://cdn.staticfile.org/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://cdn.staticfile.org/twitter-bootstrap/3.2.0/js/bootstrap.min.js"></script>
	@yield('scripts')
	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-6183853-12', 'auto');
		ga('send', 'pageview');
	</script>
</body>
</html>