@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}
		</h3>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class="col-md-4">姓名</th>
						<th class="col-md-2">类别</th>
						<th class="col-md-4">日期</th>
						<th class="col-md-2">操作</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($messages as $message)
						<tr>
							<td>
								<a href="{{{ URL::to('admin/messages/'.$message->id.'/show?page='.Input::get('page',1)) }}}">
								{{{trim($message->name) ?: '[未留名]'}}}
								</a>
							</td>
							<td> {{{$message->getType()}}} </td>
							<td> {{{$message->created_at}}} </td>
							<td>
								<a href="{{{ URL::to('admin/messages/'.$message->id.'/delete' ) }}}" class="btn btn-danger btn-xs" onclick="return confirm('确定要删除吗?');">删除</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{{ $messages->links() }}
		</div>
		@if (isset($currentMessage))
			<div class="col-xs-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{{{$currentMessage->name}}} 的留言
						<span class="label label-primary pull-right">{{{$currentMessage->getType()}}}</span>
						</h3>
					</div>
					<div class="panel-body">
						<dl>
							<dt>联系方式</dt><dd>{{{$currentMessage->contact}}}</dd>
							<dt>留言内容</dt><dd>{{{$currentMessage->content}}}</dd>
						</dl>
					</div>
				</div>
			</div>
		@endif
	</div>
@stop