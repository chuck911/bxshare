@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
	<ol class="breadcrumb">
		<li><a href="{{{ URL::to('admin/blogs/index') }}}">故事管理</a></li>
		<li class="active">故事内容编辑</li>
	</ol>
	<!-- Tabs -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab-general" data-toggle="tab">内容</a></li>
			<li><a href="#tab-meta-data" data-toggle="tab">Meta</a></li>
		</ul>
	<!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($post)){{ URL::to('admin/blogs/' . $post->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- Post Title -->
				<div class="form-group {{{ $errors->has('title') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="title">标题</label>
						<input class="form-control" type="text" name="title" id="title" value="{{{ Input::old('title', isset($post) ? $post->title : null) }}}" />
						{{ $errors->first('title', '<span class="help-block">:message</span>') }}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">一句话简介</label>
						<input class="form-control" type="text" name="intro" value="{{{ Input::old('intro', isset($post) ? $post->intro : null) }}}" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">类型</label>
						<select class="form-control" name="type">
							@if (isset($post) && $post->type )
							@if($post->type == 1)
								<option value="1" selected>none</option>
								<option value="2" >slide</option>
							@endif
							@if($post->type == 2)	
							   {{$post->type}}
								<option value="1"> none</option>
								<option value="2" selected>slide</option>		
							@endif
							@endif	
						</select>  
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">视频地址</label>
						<input class="form-control" type="text" name="video" value="{{{ Input::old('video', isset($post) ? $post->video : null) }}}" />
					</div>
				</div>
				<!-- ./ post title -->
				<div class="form-group">
					<div class="col-md-12">
						<label>缩略图</label>
						<div>
							<input id="upload-token" type="hidden" value="{{{ $uploadToken }}}">
							<input id="image-url" name="image-url" type="hidden">
							<div id="image-uploaded" style="margin-bottom:10px;">
								@if ( isset($post) && $post->image )
								<img src="{{{ $post->image }}}!300x200">
								@endif
							</div>
							<button id="btn-imagecloud" class="btn btn-primary" type="button">上传</button>
						</div>
					</div>
				</div>
				<!-- Content -->
				<div class="form-group {{{ $errors->has('content') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="content">内容</label>
						<textarea class="form-control full-width editor" name="content" value="content" rows="10">{{{ Input::old('content', isset($post) ? $post->content : null) }}}</textarea>
						{{{ $errors->first('content', '<span class="help-block">:message</span>') }}}
					</div>
				</div>
				<!-- ./ content -->
			</div>
			<!-- ./ general tab -->

			<!-- Meta Data tab -->
			<div class="tab-pane" id="tab-meta-data">
				<!-- Meta Title -->
				<div class="form-group {{{ $errors->has('meta-title') ? 'error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="meta-title">Meta Title</label>
						<input class="form-control" type="text" name="meta-title" id="meta-title" value="{{{ Input::old('meta-title', isset($post) ? $post->meta_title : null) }}}" />
						{{{ $errors->first('meta-title', '<span class="help-block">:message</span>') }}}
					</div>
				</div>
				<!-- ./ meta title -->

				<!-- Meta Description -->
				<div class="form-group {{{ $errors->has('meta-description') ? 'error' : '' }}}">
					<div class="col-md-12 controls">
                        <label class="control-label" for="meta-description">Meta Description</label>
						<input class="form-control" type="text" name="meta-description" id="meta-description" value="{{{ Input::old('meta-description', isset($post) ? $post->meta_description : null) }}}" />
						{{{ $errors->first('meta-description', '<span class="help-block">:message</span>') }}}
					</div>
				</div>
				<!-- ./ meta description -->

				<!-- Meta Keywords -->
				<div class="form-group {{{ $errors->has('meta-keywords') ? 'error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="meta-keywords">Meta Keywords</label>
						<input class="form-control" type="text" name="meta-keywords" id="meta-keywords" value="{{{ Input::old('meta-keywords', isset($post) ? $post->meta_keywords : null) }}}" />
						{{{ $errors->first('meta-keywords', '<span class="help-block">:message</span>') }}}
					</div>
				</div>
				<!-- ./ meta keywords -->
			</div>
			<!-- ./ meta data tab -->
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<button type="submit" class="btn btn-success">保存</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
@section('styles')
	<link rel="stylesheet" type="text/css" href="http://cdn.staticfile.org/font-awesome/4.1.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/simditor.css')}}">
@stop
@section('scripts')
	<script src="{{asset('assets/js/module.min.js')}}"></script>
	<script src="{{asset('assets/js/uploader.min.js')}}"></script>
	<script src="{{asset('assets/js/hotkeys.min.js')}}"></script>
	<script src="{{asset('assets/js/simditor.min.js')}}"></script>
	<script src="{{asset('assets/js/plupload.full.js')}}"></script>
	<script>
		$(function(){
			var toolbar = ['title', 'bold', 'italic', 'underline', 'strikethrough', 'color', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent'];
			var editor = new Simditor({
				textarea: $('.editor'),
				toolbar: toolbar,
				toolbarFloat: true,
				// toolbarFloatOffset: "50px",
				pasteImage: true,
				upload: {
					url:'/admin/blogs/uploadimage',
					params: {_token:'{{{ csrf_token() }}}'},
					pasteImage: true
				}
			});

			var urlRoot = 'http://7mnmqf.com2.z0.glb.clouddn.com/';
			var suffix = '!300x200';
			var uploader = new plupload.Uploader({
				runtimes : 'html5,html4',
				browse_button : 'btn-imagecloud',
				multi_selection : true,
				max_file_size : '2mb',
				file_data_name: 'file',
				url : 'http://up.qiniu.com',
				multipart_params : {token:$('#upload-token').val()},
				filters : [
					{title : "Image files", extensions : "jpg,gif,png"}
				]
			});
			uploader.init();

			uploader.bind('FilesAdded', function(uploader, files) {
				uploader.start();
			});

			uploader.bind('FileUploaded',function(uploader,file,response){
				var data = $.parseJSON(response.response);
				var image = urlRoot + data.key + suffix;
				$('#image-uploaded').html($('<img>').attr('src',image));
				$('#image-url').val(urlRoot + data.key);
			});
		});
	</script>
@stop