@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}

			<div class="pull-right">
				<a href="{{{ URL::to('admin/blogs/create') }}}" class="btn btn-small btn-info iframe"><span class="glyphicon glyphicon-plus-sign"></span> 编写新故事</a>
			</div>
		</h3>
	</div>

	<table id="blogs" class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-7">标题</th>
				<th class="col-md-2">日期</th>
				<th class="col-md-3">操作</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($posts as $post)
				<tr>
					<td> <a target="_blank" href="{{{$post->url()}}}">{{{$post->title}}}</a> </td>
					<td> {{{$post->created_at}}} </td>
					<td>
						<a href="{{{ URL::to('admin/blogs/'.$post->id.'/edit' ) }}}" class="btn btn-primary btn-sm">编辑</a>
						@if ($post->star)
							<a href="{{{ URL::to('admin/blogs/'.$post->id.'/unstar' ) }}}" class="btn btn-primary btn-sm">取消推荐</a>
						@else
							<a href="{{{ URL::to('admin/blogs/'.$post->id.'/star' ) }}}" class="btn btn-primary btn-sm">推荐</a>
						@endif
						@if ($post->status==1)
							<a href="{{{ URL::to('admin/blogs/'.$post->id.'/unpublish' ) }}}" class="btn btn-danger btn-sm">取消发布</a>
						@else
							<a href="{{{ URL::to('admin/blogs/'.$post->id.'/publish' ) }}}" class="btn btn-success btn-sm">发布</a>
						@endif
						<a href="{{{ URL::to('admin/blogs/'.$post->id.'/delete' ) }}}" class="btn btn-danger btn-xs" onclick="return confirm('确定要删除吗?');">删除</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{ $posts->links() }}
@stop